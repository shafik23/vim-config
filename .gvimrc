" Set initial window dimensions
set lines=40 columns=120

if has("gui_gtk2")
   set guifont=Monospace\ 11
elseif has("gui_photon")
   set guifont=Monospace:s11
elseif has("gui_kde")
   set guifont=Courier\ New/11/-1/5/50/0/0/0/1/0
elseif has("x11")
   set guifont=-*-courier-medium-r-normal-*-*-180-*-*-m-*-*
else
   set guifont=Consolas:h13:cANSI
endif

if filereadable(glob("~/.site_gvimrc"))
   source ~/.site_gvimrc
endif
