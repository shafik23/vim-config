set nocompatible


"------------------------------------------------------
" Conditional commands based on Vim flavor
" Note: On Windows, remember to set the "$HOME" env variable.
"------------------------------------------------------
if has("gui_running")
   if has('win32') || has('win64')
      set runtimepath=$HOME/.vim,$VIM/vimfiles,$VIMRUNTIME,$VIM/vimfiles/after,$HOME/.vim/after
   endif
else
endif


"------------------------------------------------------
" Tabs and backspace/delete
"------------------------------------------------------
set expandtab
set tabstop=4
set shiftwidth=4
set backspace=2
set t_kD=[3~


"------------------------------------------------------
" Keyboard mappings
"------------------------------------------------------
" Nicer memory-jump points
nnoremap ' `
nnoremap ` '

" For use with Ack.vim - ack for current cursor word
nnoremap <c-f> :Ack!<cr>

" For use with Ack.vim - ack for current visual selection
vnoremap <c-f> "zy:Ack! <c-r>=fnameescape(@z)<cr><cr>

" Grep for the currently highlighted selection
vnorem // y/<c-r>"<cr>

" Make Y behave similarly to D and C
nnoremap Y y$

" Allow writing via sudo when permissions are prohibitive
cnoremap w!! w !sudo tee > /dev/null %

" Insert empty lines in normal mode
"nnoremap <cr> O<Esc>

" Get a list of open buffers; press buffer # to switch to it
nnoremap <F2> :ls<cr>:b

" Generate a C/C++ ctags file in the current directy
nnoremap <F9> :!ctags -R --c++-kinds=+p --fields=+iaS --extra=+q .

" Generate a JS tags file in the current directy
nnoremap <F8> :!LC_COLLATE=C find . -type f -iregex .*\.js$ -not -path "./node_modules/*" -exec jsctags {} -f \; \| sed '/^$/d' \| sort > tags


"------------------------------------------------------
" Miscellanous settings
"------------------------------------------------------
set mouse=a                         " nicer mouse support in terminal-mode (use shift-select to copy out)
set wildmenu                        " auto-completion menu in cmd mode.
set background=dark
set history=2000
set hlsearch                        " highlight search results
set noignorecase
set noincsearch
set shortmess+=r
set showcmd
set nowrap                          " don't wrap lines
set number
set hidden                          " better buffer management
set showmode
set nosmartcase
set spelllang=en_us
"set spell                          " Enable spell-checking
set title                           " set title of xterm
set tw=120                          " right margin
set splitbelow                      " make window splits more natural
set splitright
set vb                              " visual bell instead of annoying beep
set t_vb=                           " this disables the visual bell

syntax on

" when saving vim sessions, don't hold on to too much state.
set ssop-=options    " do not store global and local values in a session
set ssop-=folds      " do not store folds

"------------------------------------------------------
" Auto-commands:
"------------------------------------------------------
filetype plugin on

autocmd BufRead *.clj set autoindent
autocmd BufWritePre * :%s/\s\+$//e | exec "normal $"
autocmd BufNewFile,BufRead *.json set filetype=javascript
autocmd BufNewFile,BufRead *.mustache set filetype=html

autocmd FileType php,javascript,cs,c,cpp,slang,java set cindent autoindent
autocmd FileType sql set autoindent
autocmd FileType make set autoindent
autocmd FileType sh set smartindent
autocmd FileType sh set cinwords+=then

autocmd FileType go set autoindent
autocmd FileType go nmap <leader>r <Plug>(go-run)
autocmd FileType go nmap <leader>b <Plug>(go-build)
autocmd FileType go nmap <leader>t <Plug>(go-test)
autocmd FileType go nmap <leader>i <Plug>(go-info)

" Tell vim to auto-reload files that have been modified externally.
" Note that this only works in Normal mode, and is generally
" considered to be a 'hack'.
set autoread
autocmd CursorHold * checktime


"------------------------------------------------------
" Vundle - Dynamically manage plugins; depends on 'git'
"------------------------------------------------------
filetype off
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

Plugin 'gmarik/Vundle.vim'
Plugin 'vim-airline/vim-airline'
"Plugin 'vim-airline/vim-airline-themes'
Plugin 'scrooloose/nerdtree'
Plugin 'kien/ctrlp.vim'
Plugin 'vim-scripts/taglist.vim'
Plugin 'mileszs/ack.vim'
Plugin 'artoj/qmake-syntax-vim'
Plugin 'peterhoeg/vim-qml'
"Plugin 'octol/vim-cpp-enhanced-highlight'
Plugin 'fatih/vim-go'
Plugin 'klen/python-mode'
Plugin 'vim-scripts/OmniCppComplete'
"Plugin 'tikhomirov/vim-glsl'
Plugin 'tpope/vim-surround'
Plugin 'othree/yajs.vim'
Plugin 'morhetz/gruvbox'
"Plugin 'keith/swift.vim'

" YouCompleteMe is very heavy-weight and requires extra
" steps to install. If you're running on a weak computer, disable it.
"Plugin 'Valloric/YouCompleteMe'

call vundle#end()
filetype off
filetype plugin indent on


"------------------------------------------------------
" Plugin specific settings / setup
"------------------------------------------------------

" For YouCompleteMe (that bloated piece of crap)
let g:ycm_global_ycm_extra_conf = $HOME."/.vim/bundle/YouCompleteMe/third_party/ycmd/cpp/ycm/.ycm_extra_conf.py"

" For vim-airline
set laststatus=2
set noshowmode
let g:airline#extensions#whitespace#checks = []

" For Ctrl-P
let g:ctrlp_working_path_mode=''    " default setting is 'ra'. 'c' is also possible
let g:ctrlp_max_files=0             " unlimited number of files scanned

" For vim-go
let g:go_fmt_autosave = 0

" For PyMode
let g:pymode_lint_options_mccabe = { 'complexity': 25 }
let g:pymode_options_max_line_length = 130


"------------------------------------------------------
" Custom Functions
"------------------------------------------------------
function! GrepBuffers(word)
   call setqflist([])
   let command = 'silent! bufdo vimgrepadd /' . a:word . '/j %'
   let current_buf = bufnr('%')
   exec command
   exec 'b' current_buf
   copen
endfunction

" Shortcut/command for grepping for word under cursor in all open buffers
command! -nargs=+ GrepBuf :call GrepBuffers(<f-args>)
nnoremap <F12> :GrepBuf <c-r><c-w> <cr>


"------------------------------------------------------
" Process helptags
"------------------------------------------------------
helptags ~/.vim/doc/


"------------------------------------------------------
" Set the colorscheme to desert for default, then something fancier if available
"------------------------------------------------------
colorscheme desert
silent! colorscheme gruvbox


"------------------------------------------------------
" Source site specific vimrc file, if present:
"------------------------------------------------------
if filereadable(glob("~/.site_vimrc"))
   source ~/.site_vimrc
endif
