On linux:
- Symlink the following files/folders from the home directory to their counterparts in the Dropbox vim folder:
    - .vimrc
    - .gvimrc
    - .vim/

On windows:
- Simply set the "HOME" env variable to be the Dropbox vim directory (parent of .vim/ folder).
    - WARNING: this may cause problems if there are other programs assuming this HOME directory.
