# Summary #
This is a minimal vim configuration environment.

To setup, download then run "install_vim_config.sh". This will copy files to your ~ and ~/.vim directories. If a file already exists on your machine, then you must type 'y' to confirm copy. Keep in mind that even if you don't overwrite any files you had, your configuration may break, so use at your own risk.

Your .vimrc file will be backed up then overwritten automatically.


# Dependencies #
1. Vim (duh)
2. Ack (the enhanced grep utility)
3. Exuberant tags (i.e. ctags)
4. CScope (cscope)
5. Git (so that Vundle can correctly manage other plugins).