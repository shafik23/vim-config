#!/bin/bash


echo "This script will install plugins, colorschemes, docs, etc into your .vim directory."
echo "Overwrites will have to be confirmed, but keep in mind that this may alter your "
echo "environment substationally."
echo
echo "Are you sure you want to continue? (type 'yes' to confirm)"
read decision

if [[ $decision != "yes" ]]
then
   echo "Aborting ..."
   exit 1
fi


echo
echo "Backing up vimrc, and installing new one ..."
cp ~/.vimrc ~/.vimrc.back
cp .vimrc ~/

echo "Backing up gvimrc, and installing new one ..."
cp ~/.gvimrc ~/.gvimrc.back
cp .gvimrc ~/

echo
echo "Copying plugins, docs, and colorschemes ..."
mkdir -p ~/.vim/
cp -r .vim/* ~/.vim/

echo 
echo Done
